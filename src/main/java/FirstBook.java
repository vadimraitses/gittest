import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import static java.lang.System.out;

/**
 * Created by vadim.r on 21/06/2017.
 */


public class FirstBook {
    public FirstBook() {



        // PrintFirst();
        // PrintSecond();
        // PrintThird();
        // Variables();
        // MathematicalOperations();
        // ReadHumanInput();
        // StoreHumanInput();
        // Calculation();
        // VariablesHoldValues();
        // VariableModification();
        // BooleanOperations();
        // ComparingStrings();
        // MoreIfstatements();
        // IfElse();
        // ChunkedIfElse();
        // MoreIFElse();
        // CaseSwitch();
        // CompareStringsExample();
        // RandomNumbers();
        // ThreeRandomNumbers();
        // WhileLoop();
        // NumberGuessgame();
        // DoWhileLoop();
        // CreateSumWhile();
        // int a  =  GetInt(0);
        // MakeSecuredPasswordSHA256();
        // WriteTextFile();
        // ReadTextFile();
        // SaveScores();
        // firstFor();
        //////////////////////////////////////////////Second PART BOOK////////////////////////////
        //Second Solution

    }

    private static void firstFor() {


        int i = 0;   ///Commit
        for (; i < 5; i++) {

            System.out.println("High score is " + i);

        }

    }

    private static void SaveScores() {

        Scanner keyboard = new Scanner(System.in);
        String coin, again, bestName, saveFileName = "coin-flip-score.txt";
        int streak = 0, best;
        boolean gotHeads;
        try {
            File f = new File(saveFileName);
            if (f.exists() && f.length() > 0) {
                Scanner input = new Scanner(f);
                bestName = input.next();
                best = input.nextInt();
                input.close();
                System.out.print("High score is " + best);
                System.out.println(" flips in a row by " + bestName);
            } else {
                System.out.println("Save game file doesn't exist or is empty.");
                best = -1;
                bestName = "";
            }
            do {
                gotHeads = Math.random() < 0.5;
                if (gotHeads)
                    coin = "HEADS";
                else
                    coin = "TAILS";
                System.out.println("You flip a coin and it is... " + coin);
                if (gotHeads) {
                    streak++;
                    System.out.println("\tThat's " + streak + " in a row....");
                    System.out.print("\tWould you like to flip again (y/n)? ");
                    again = keyboard.next();
                } else {
                    streak = 0;
                    again = "n";
                }
            } while (again.equals("y"));
            System.out.println("Final score: " + streak);
            if (streak > best) {
                System.out.println("That's a new high score!");
                System.out.print("Your name: ");
                bestName = keyboard.next();
                best = streak;
            } else if (streak == best) {
                System.out.println("That ties the high score. Cool.");
            } else {
                System.out.print("You'll have to do better than ");
                System.out.println(streak + " if you want a high score.");
            }
// Save this name and high score to the file.
            PrintWriter out = new PrintWriter(f);
            out.println(bestName);
            out.println(best);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void ReadTextFile() {


        String name;
        int a, b, c, sum;
        System.out.print("Getting name and three numbers from file...");
        try {
            Scanner fileIn = new Scanner(new File("name-and-numbers.txt"));
            name = fileIn.nextLine();
            a = fileIn.nextInt();
            b = fileIn.nextInt();
            c = fileIn.nextInt();
            fileIn.close();
            System.out.println("done.");
            System.out.println("Your name is " + name);
            sum = a + b + c;
            System.out.println(a + "+" + b + "+" + c + " = " + sum);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void WriteTextFile() {
        PrintWriter fileout;
        try {
            fileout = new PrintWriter("receipt.txt");
            fileout.println("+------------------------+");
            fileout.println("|                        |");
            fileout.println("| CORNER STORE           |");
            fileout.println("|                        |");
            fileout.println("| 2014-06-25 04:38PM     |");
            fileout.println("|                        |");
            fileout.println("| Gallons: 12.464        |");
            fileout.println("| Price/gallon: $ 3.459  |");
            fileout.println("|                        |");
            fileout.println("| Fuel total: $ 43.11    |");
            fileout.println("|                        |");
            fileout.println("+------------------------+");
            fileout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        catch (IOException err ) {
//            System.out.println("Sorry, I can't open 'receipt.txt' for writing.");
//            System.out.println("Maybe the file exists and is read-only?");
//            fileout = null;
//            System.exit(1);
//        }

    }


    public static void MakeSecuredPasswordSHA256() {

        try {
            Scanner keyboard = new Scanner(System.in);
            String pw, hash;

            System.out.print("Password: ");
            pw = keyboard.nextLine();
            MessageDigest digest = null;
            for (int i = 0; i < 50; i++) {
                String[] str = new String[]{"SHA-256", "SHA-1", "MD5"};
                int CurrentHashEncription = (int) (3 * Math.random());
                System.out.print("number rolled is : " + CurrentHashEncription + "\t");
                digest = MessageDigest.getInstance(str[CurrentHashEncription]);

                digest.update(pw.getBytes("UTF-8"));
                hash = DatatypeConverter.printHexBinary(digest.digest());
                System.out.println(hash + "\t by " + str[CurrentHashEncription]);
                digest = null;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    /**
     * @param i integer to pass
     * @return integer which passed a parameter
     * @author Vadim Raitses
     */
    public static int GetInt(int i) {
        return i;
    }

    private static void CreateSumWhile() {
        int totalSum = 0, current;

        Scanner keyboard = new Scanner(System.in);

        System.out.print("Type in a bunch of values and I'll add them up. ");
        System.out.println("I'll stop when you type a zero.");
        System.out.print("Value: ");
        current = keyboard.nextInt();
        while (current != 0) {
            totalSum += current;
            System.out.println("The total so far is: " + totalSum);
            System.out.print("Value: ");
            current = keyboard.nextInt();
        }
        System.out.println("The final total is: " + totalSum);

    }

    private static void DoWhileLoop() {

        Scanner keyboard = new Scanner(System.in);
        String coin, again;
        int streak = 0;
        do {

            if (Math.random() < 0.5) {
                coin = "HEADS";
                streak++;
                System.out.println("\tThat's " + streak + " in a row....");
                System.out.print("\tWould you like to flip again (y/n)? ");
                again = keyboard.next();
            } else {
                coin = "TAILS";
                System.out.println("\tYou lose everything!");
                System.out.println("\tShould've quit while you were aHEAD....");
                streak = 0;
                again = "n";
            }
            System.out.println("You flip a coin and it is... " + coin);
        } while (again.equals("y"));
        System.out.println("Final score: " + streak);

    }

    private static void NumberGuessgame() {

        Scanner sc = new Scanner(System.in);

        int secret, guess;

        secret = 1 + (int) (100 * Math.random());

        System.out.println("I'm thinking of a number between 1-100.");
        System.out.println("Try to guess it!");
        System.out.print("> ");
        guess = sc.nextInt();

        while (secret != guess) {

            if (guess > secret)
                System.out.print("Sorry, your guess is too high.");

            if (guess < secret)
                System.out.print("Sorry, your guess is too low.");

            System.out.print(" Try again.\n> ");
            guess = sc.nextInt();


        }

        System.out.println("You guessed it! What are the odds?!?");
    }

    private static void WhileLoop() {

        Scanner keyboard = new Scanner(System.in);
        int pin, entry;
        pin = 12345;
        System.out.println("WELCOME TO THE BANK OF JAVA.");
        System.out.print("ENTER YOUR PIN: ");
        entry = keyboard.nextInt();
        while (entry != pin) {
            System.out.println("\nINCORRECT PIN. TRY AGAIN.");
            System.out.print("ENTER YOUR PIN: ");
            entry = keyboard.nextInt();
        }
        System.out.println("\nPIN ACCEPTED. YOUR ACCOUNT BALANCE IS $425.17");

    }

    private static void ThreeRandomNumbers() {

        System.out.println("first 1-3 range:\t " + (1 + (int) (3 * Math.random())));
        System.out.println("first 1-3 range:\t " + (1 + (int) (3 * Math.random())));
        System.out.println("first 1-3 range:\t " + (1 + (int) (3 * Math.random())));


        System.out.println("first 5-10 range:\t " + (5 + (int) (6 * Math.random())));
        System.out.println("first 5-10 range:\t " + (5 + (int) (6 * Math.random())));
        System.out.println("first 5-10 range:\t " + (5 + (int) (6 * Math.random())));

    }

    private static void RandomNumbers() {

        int a, b, c, d;
        double r, rps;
        rps = Math.random();
        if (rps < 0.3333333) { // will be true 1/3 of the time
            System.out.println("ROCK");
        } else if (rps < 0.6666667) {
            System.out.println("PAPER");
        } else {
            System.out.println("SCISSORS");
        }
// pick four random integers, each 1-10
        a = 1 + (int) (10 * Math.random());
        b = 1 + (int) (10 * Math.random());
        c = 1 + (int) (10 * Math.random());
        d = 1 + (int) (10 * Math.random());
        System.out.println("1-10:\t" + a + "\t" + b + "\t" + c + "\t" + d);
// pick four random integers, each 1-6
        a = 1 + (int) (6 * Math.random());
        b = 1 + (int) (6 * Math.random());
        c = 1 + (int) (6 * Math.random());
        d = 1 + (int) (6 * Math.random());


        System.out.println("1-6:\t" + a + "\t" + b + "\t" + c + "\t" + d);
// pick a single random integer, 1-100
        a = 1 + (int) (100 * Math.random());
        System.out.println("1-100:\t" + a + "\t" + a + "\t" + a + "\t" + a);
// pick four random integers, each 1-100
        a = 1 + (int) (100 * Math.random());
        b = 1 + (int) (100 * Math.random());
        c = 1 + (int) (100 * Math.random());
        d = 1 + (int) (100 * Math.random());
        System.out.println("1-100:\t" + a + "\t" + b + "\t" + c + "\t" + d);
// pick four random integers, each 0-99
        a = 0 + (int) (100 * Math.random());
        b = 0 + (int) (100 * Math.random());
        c = (int) (100 * Math.random());
        d = (int) (100 * Math.random());
        System.out.println("0-99:\t" + a + "\t" + b + "\t" + c + "\t" + d);
// pick four random integers, each 10-20
        a = 10 + (int) (11 * Math.random());
        b = 10 + (int) (11 * Math.random());
        c = 10 + (int) (11 * Math.random());
        d = 10 + (int) (11 * Math.random());
        System.out.println("10-20:\t" + a + "\t" + b + "\t" + c + "\t" + d);
// display four random doubles, each [0-1)
        System.out.println("0-1:\t" + Math.random() + "\t" + Math.random());
        System.out.println("0-1:\t" + Math.random() + "\t" + Math.random());
        r = 10 * Math.random();
        System.out.println("0-9.99:\t" + r);
        System.out.println("0-9:\t" + (int) r);
        System.out.println("1-10:\t" + (1 + (int) r));


    }

    private static void CompareStringsExample() {

        Scanner out = new Scanner(System.in);

        System.out.println("Which language");

        String name_lang = out.next();

        if (name_lang.compareTo("c++") > 0)
            System.out.println("Gerater");
        if (name_lang.compareTo("c++") == 0)
            System.out.println("Equal");
        if (name_lang.compareTo("c++") < 0)
            System.out.println("Smaller");

    }


    private static void CaseSwitch() {

        Scanner keyboard = new Scanner(System.in);
        int month, days;
        String monthName;
        System.out.print("Which month? (1-12) ");
        month = keyboard.nextInt();
        switch (month) {
            case 1:
                monthName = "January";
                break;
            case 2:
                monthName = "February";
                break;
            case 3:
                monthName = "March";
                break;
            case 4:
                monthName = "April";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "June";
                break;
            case 7:
                monthName = "July";
                break;
            case 8:
                monthName = "August";
                break;
            case 9:
                monthName = "September";
                break;
            case 10:
                monthName = "October";
                break;
            case 11:
                monthName = "November";
                break;
            case 12:
                monthName = "December";
                break;
            default:
                monthName = "error";
        }

        switch (month) {
            case 9:
            case 4:
            case 6:
            case 11:
                days = 30;
                break;
            case 2:
                days = 28;
                break;
            default:
                days = 31;
        }
        System.out.println(days + " days hath " + monthName);

    }

    private static void MoreIFElse() {

        Scanner keyboard = new Scanner(System.in);
        String title;
        System.out.print("First name: ");
        String first = keyboard.next();
        System.out.print("Last name: ");
        String last = keyboard.next();
        System.out.print("Gender (M/F): ");
        String gender = keyboard.next();
        System.out.print("Age: ");
        int age = keyboard.nextInt();
        if (age < 20) {
            title = first;
        } else {
            if (gender.equals("F")) {
                System.out.print("Are you married, " + first + "? (Y/N): ");
                String married = keyboard.next();
                if (married.equals("Y")) {
                    title = "Mrs.";
                } else {
                    title = "Ms.";
                }
            } else {
                title = "Mr.";
            }
        }
        System.out.println("\n" + title + " " + last);

    }

    private static void ChunkedIfElse() {
        Scanner keyboard = new Scanner(System.in);
        double bmi;
        System.out.print("Enter your BMI: ");
        bmi = keyboard.nextDouble();
        out.print("BMI category: ");
        if (bmi < 15.0) {
            System.out.println("very severely underweight");
        } else if (bmi <= 16.0) {
            System.out.println("severely underweight");
        } else if (bmi < 18.5) {
            System.out.println("underweight");
        } else if (bmi < 25.0) {
            System.out.println("normal weight");
        } else if (bmi < 30.0) {
            System.out.println("overweight");
        } else if (bmi < 35.0) {
            System.out.println("moderately obese");
        } else if (bmi < 40.0) {
            System.out.println("severely obese");
        }
// else {
//            System.out.println("very severely/\"morbidly\" obese");
//        }

    }

    private static void IfElse() {

        int age = 20;
        boolean onGuestList = false;
        double allure = 7.5;
        String gender = "F";
        if (onGuestList || age >= 21 || (gender.equals("F") && allure >= 8)) {
            System.out.println("You are allowed to enter the club.");
        } else {
            System.out.println("You are not allowed to enter the club.");
        }

    }

    private static void MoreIfstatements() {

        Scanner keyboard = new Scanner(System.in);
        double first, second;
        System.out.print("Give me two numbers. First: ");
        first = keyboard.nextDouble();
        System.out.print("Second: ");
        second = keyboard.nextDouble();
        if (first < second) {
            System.out.println(first + " is LESS THAN " + second);
        }
        if (first <= second) {
            System.out.println(first + " is LESS THAN/EQUAL TO " + second);
        }
        if (first == second) {
            System.out.println(first + " is EQUAL TO " + second);
        }
        if (first >= second) {
            System.out.println(first + " is GREATER THAN/EQUAL TO " + second);
        }
        if (first > second) {
            System.out.println(first + " is GREATER THAN " + second);
        }
        if (first != second)
            System.out.println(first + " is NOT EQUAL TO " + second);


    }

    private static void ComparingStrings() {

        boolean a, b;
        //a = ("cat" < "dog");
        b = ("horse" == "horse");


        Scanner keyboard = new Scanner(System.in);
        String word;
        boolean yep, nope;
        System.out.println("Type the word \"weasel\", please.");
        word = keyboard.next();


        yep = word.equals("weasel");
        nope = !word.equals("weasel");
        System.out.println("You typed what was requested: " + yep);
        System.out.println("You ignored polite instructions: " + nope);
    }

    private static void BooleanOperations() {
        Scanner keyboard = new Scanner(System.in);
        boolean a, b, c, d, e, f;
        double x, y;
        System.out.print("Give me two numbers. First: ");
        x = keyboard.nextDouble();
        System.out.print("Second: ");
        y = keyboard.nextDouble();
        a = (x < y);
        b = (x <= y);
        c = (x == y);
        d = (x != y);
        e = (x > y);
        f = (x >= y);
        System.out.println(x + " is LESS THAN " + y + ": " + a);

        System.out.println(x + " is LESS THAN / EQUAL TO " + y + ": " + b);
        System.out.println(x + " is EQUAL TO " + y + ": " + c);
        System.out.println(x + " is NOT EQUAL TO " + y + ": " + d);
        System.out.println(x + " is GREATER THAN " + y + ": " + e);
        System.out.println(x + " is GREATER THAN / EQUAL TO " + y + ": " + f);
        System.out.println();
        System.out.println(!(x < y) + " " + (x >= y));
        System.out.println(!(x <= y) + " " + (x > y));
        System.out.println(!(x == y) + " " + (x != y));
        System.out.println(!(x != y) + " " + (x == y));
        System.out.println(!(x > y) + " " + (x <= y));
        System.out.println(!(x >= y) + " " + (x < y));


    }

    private static void VariableModification() {

        int i, j, k;
        i = 5;
        j = 5;
        k = 5;
        System.out.println("i: " + i + "\tj: " + j + "\tk: " + k);
        i = i + 3;
        j = j - 3;
        k = k * 3;
        System.out.println("i: " + i + "\tj: " + j + "\tk: " + k + "\n");
        i = 5;
        j = 5;
        k = 5;
        System.out.println("i: " + i + "\tj: " + j + "\tk: " + k);
        i += 3;
        j -= 3;
        k *= 3;
        System.out.println("i: " + i + "\tj: " + j + "\tk: " + k + "\n");
        i = j = k = 5;
        System.out.println("i: " + i + "\tj: " + j + "\tk: " + k);
        i += 1;
        j -= 2;
        k *= 3;
        System.out.println("i: " + i + "\tj: " + j + "\tk: " + k + "\n");
        i = j = 5;
        System.out.println("i: " + i + "\tj: " + j);
        i = +1; // Oops!
        j = -2;
        System.out.println("i: " + i + "\tj: " + j + "\n");
        i = j = 5;
        System.out.println("i: " + i + "\tj: " + j);
        i++;
        j--;
        System.out.println("i: " + i + "\tj: " + j);


    }

    private static void VariablesHoldValues() {

        Scanner keyboard = new Scanner(System.in);
        double price /* = 0*/, salesTax, total;
        System.out.print("How much is the purchase price? ");
        price = keyboard.nextDouble();
        salesTax = price * 0.0825;
        total = price + salesTax;
        System.out.println("Item price:\t" + price);
        System.out.println("Sales tax:\t" + salesTax);
        System.out.println("Total cost:\t" + total);

    }

    private static void Calculation() {

        Scanner keyboard = new Scanner(System.in);
        double m, kg, bmi;
        System.out.print("Your height in m: ");
        m = keyboard.nextDouble();
        System.out.print("Your weight in kg: ");
        kg = keyboard.nextDouble();
        bmi = kg / (m * m);
        System.out.println("Your BMI is " + bmi);

    }

    private static void StoreHumanInput() {

        String name;
        int age;
        double weight, income;


        Scanner keyboard = new Scanner(System.in);

        System.out.print("Check");
        income = keyboard.nextDouble();

        System.out.print("Hello. What is your name? ");
        name = keyboard.next();
        System.out.print("Hi, " + name + "! How old are you? ");
        age = keyboard.nextInt();
        System.out.println("So you're " + age + ", eh? That's not very old.");
        System.out.print("How much do you weigh, " + name + "? ");
        weight = keyboard.nextDouble();
        System.out.println(weight + "! Better keep that quiet!!");
        System.out.print("Finally, what's your income, " + name + "? ");
        income = keyboard.nextDouble();
        System.out.print("Hopefully that is " + income + " per hour");
        System.out.println(" and not per year!");
        System.out.print("Well, thanks for answering my rude questions, ");
        System.out.println(name + ".");

    }

    private static void ReadHumanInput() {

        Scanner keyboard = new Scanner(System.in);
        System.out.println("What city is the capital of France?");
        keyboard.next();
        System.out.println("What is 6 multiplied by 7?");
        keyboard.nextInt();
        System.out.println("Enter a number between 0.0 and 1.0.");
        keyboard.nextDouble();
        System.out.println("Is there anything else you would like to say?");
        keyboard.next();

    }

    private static void MathematicalOperations() {


        int a, b, c, d, e, f, g;
        double x, y, z;
        String one, two, both;
        a = 10;
        b = 27;
        System.out.println("a is " + a + ", b is " + b);
        c = a + b;
        System.out.println("a+b is " + c);
        d = a - b;
        System.out.println("a-b is " + d);
        e = a + b * 3;
        System.out.println("a+b*3 is " + e);
        f = b / 2;
        System.out.println("b/2 is " + f);
        g = b % 10;
        System.out.println("b%10 is " + g);
        x = 1.1;
        System.out.println("\nx is " + x);
        y = x * x;
        System.out.println("x*x is " + y);
        z = b / 2;
        System.out.println("b/2 is " + z);
        System.out.println();
        one = "dog";
        two = "house";
        both = one + two;
        System.out.println(both);
        System.out.println("1.1*1.1 =" + 1.1 * 1.1);


    }

    private static void Variables() {
        int x, y, age;
        double seconds, e, checking;
        String firstName, lastName, title;
        x = 10;
        y = 400;
        age = 39;


        seconds = 4.71;
        e = 2.71828182845904523536;
        checking = 1.89;
        firstName = "Graham";
        lastName = "Mitchell";
        title = "Mr.";
        System.out.println("The variable x contains " + x);
        System.out.println("The value " + y + " is stored in the variable y.");
        System.out.println("The experiment took " + seconds + " seconds.");
        System.out.println("A favorite irrational # is Euler's number: " + e);
        System.out.println("Hopefully you have more than $" + checking + "!");
        System.out.println("My name's " + title + " " + firstName + lastName);

    }


    public static void PrintFirst() {


        System.out.println("+------------------------+");
        System.out.println("|                        |");
        System.out.println("|     CORNER STORE       |");
        System.out.println("|                        |");
        System.out.println("| 2015-03-29 04:38PM     |");
        System.out.println("|                        |");
        System.out.println("| Gallons: 10.870        |");
        System.out.println("| Price/gallon: $ 2.089  |");
        System.out.println("|                        |");
        System.out.println("| Fuel total: $ 22.71    |");
        System.out.println("|                        |");
        System.out.println("+------------------------+");

    }


    public static void PrintSecond() {


        System.out.println("Alpha");
        System.out.println("Bravo");
        System.out.println("Charlie");
        System.out.println("Delta");
        System.out.println();
        System.out.print("Echo");
        System.out.print("Foxtrot");
        System.out.println("Golf");
        System.out.print("Hotel");
        System.out.println();
        System.out.println("India");
        System.out.println();
        System.out.println("This" + " " + "is" + " " + "a" + " test.");

    }


    private static void PrintThird() {

        // This exercise demonstrates escape sequences & comments (like these)!
        System.out.print("Learn\tJava\n\tthe\nHard\tWay\n\n");
        System.out.print("\tLearn Java the \"Hard\" Way!\n");
        // System.out.frimp( "Learn Java the Hard Way" );
        System.out.print("Hello\n"); // This line prints Hello.
        System.out.print("Jello\by\n"); // This line prints Jelly.
        /* The quick brown fox jumped over a lazy dog.
        Quick wafting zephyrs vex bold Jim. */
        System.out /* testing */.println("Hard to believe, eh?");
        System.out.println("Surprised? /* abcde */ Or what did you expect?");
        System.out.println("\\ // -=- \\ //");
        System.out.println("\\\\ \\\\\\ \\\\\\\\"); // it takes 2 to make 1
        System.out.print("I hope you understand \"escape sequences\" now.\n");
        // and comments. :)


    }

}
